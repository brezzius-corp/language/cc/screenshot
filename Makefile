# Makefile

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi
LDFLAGS=-lX11 -lpng
EXECUTABLE=screenshot
EXECUTABLE_TEST=screenshot_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: screenshot
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(LDFLAGS)

test: package screenshot_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

screenshot: init main.o
	$(CC) -o $(SRC_BIN)/screenshot $(SRC_BIN)/main.o $(CFLAGS) $(LDFLAGS)

screenshot_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)

