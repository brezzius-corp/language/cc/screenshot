#ifndef __CONSTANTS_H
#define __CONSTANTS_H

typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;

/* Erreur socket */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

/* Connexion réseau */
#define PORT 8080
#define HOSTNAME "127.0.0.1"

#endif /* __CONSTANTS_H */
