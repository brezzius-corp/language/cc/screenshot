#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <png.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

int PNG_Write(char* file_name, png_bytep *row_pointers, int width, int height) {
    png_byte color_type = 2;
    png_byte bit_depth = 8;
    png_structp png_ptr;
    png_infop info_ptr;

    int y;
    FILE *fp;

    /* Create file */
    if(!(fp = fopen(file_name, "wb"))) {
        fprintf(stderr, "File %s could not be opened for writing", file_name);
        return -1;
    }


    /* Initialize stuff */
    if(!(png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL))) {
        fprintf(stderr, "Cannot create write structure");
        fclose(fp);
        return -1;
    }

    if(!(info_ptr = png_create_info_struct(png_ptr))) {
        fprintf(stderr, "Cannot create info structure");
        fclose(fp);
        return -1;
    }

    if(setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during init io");
        fclose(fp);
        return -1;
    }

    png_init_io(png_ptr, fp);

    fclose(fp);

    /* Write header */
    if(setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during writing header");
        return -1;
    }

    png_set_IHDR(png_ptr, info_ptr, width, height, bit_depth, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);

    /* Write bytes */
    if(setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during writing bytes");
        return -1;
    }

    png_write_image(png_ptr, row_pointers);

    /* End write */
    if(setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during end of write");
        return -1;
    }

    png_write_end(png_ptr, NULL);

    /* Cleanup heap allocation */
    for(y = 0 ; y < height ; y++)
        free(row_pointers[y]);

    free(row_pointers);

    return 0;
}

int main(void) {
    Display *display = XOpenDisplay(getenv("DISPLAY"));
    Window root = DefaultRootWindow(display);

    XWindowAttributes gwa;
    XImage *image;

    int i, x, y, width, height;

    uint8_t **tab;
    uint64_t red_mask, green_mask, blue_mask, pixel;
    uint8_t green, red, blue;

    XGetWindowAttributes(display, root, &gwa);

    width = gwa.width;
    height = gwa.height;

    image = XGetImage(display, root, 0, 0, width, height, AllPlanes, ZPixmap);

    tab = malloc(height * sizeof(char *));
    for(i = 0 ; i < height ; i++)
        tab[i] = malloc(width * 3);

    red_mask = image->red_mask;
    green_mask = image->green_mask;
    blue_mask = image->blue_mask;

    for(y = 0 ; y < height ; y++) {
        for(x = 0 ; x < width ; x++) {
            pixel = XGetPixel(image,x,y);

            green = (uint8_t) (pixel & green_mask) >> 8;
            red = (uint8_t) (pixel & red_mask) >> 16;
            blue = (uint8_t) pixel & blue_mask;

            tab[y][x * 3] = red;
            tab[y][(x * 3) + 1] = green;
            tab[y][(x * 3) + 2] = blue;
        }
    }

    if(!(PNG_Write("image.png", tab, width, height)))
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

